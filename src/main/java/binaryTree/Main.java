package binaryTree;

public class Main {

  public static void main(String[] args) {
    BinaryTree b = new BinaryTree();
    b.put(6);
    b.put(4);
    b.put(8);
    b.put(3);
    b.put(5);
    b.put(7);
    b.put(9);
    System.out.print("Binary tree:");
    b.print();
    b.delete(4);
    System.out.print("\nBinary tree after deleting element:");
    b.print();
  }
}
