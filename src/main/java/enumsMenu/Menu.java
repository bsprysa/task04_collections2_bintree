package enumsMenu;

import java.util.Scanner;

public class Menu {

  public void printMenu() {
    for (WeekDays d : WeekDays.values()) {
      System.out.println(d.getIndex() + ". " + d.name());
    }
  }

  public static void main(String[] args) {
    new Menu().printMenu();
    System.out.println("Choose day: ");
    Scanner scanner = new Scanner(System.in);
    int index = Integer.parseInt(scanner.nextLine());
    System.out.println("You choose: " + WeekDays.values()[index - 1]);
    scanner.close();
  }

}
